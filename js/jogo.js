const criaJogo = (sprite) => {

    let palavraSecreta = "";
    let lacunas = [];
    let etapa = 1;

    const setPalavraSecreta = palavra => {
        if (!palavra.trim()) throw new Error("Palavra inválida!");
        
        palavraSecreta = palavra;
        criaLacunas();
        proximaEtapa();
    };
    
    const criaLacunas = () => lacunas = Array(palavraSecreta.length).fill('');
    
    const getLacunas = () => lacunas;
    
    const getEtapa = () => etapa;
    
    const proximaEtapa = () => etapa = 2;
    
    const processaChute = (chute) => {
        
        if (!chute.trim()) throw new Error("Chute inválido!");
        
        const regex = RegExp(chute,"gi");
        let erro = true,
            resp;
        
        while (resp = regex.exec(palavraSecreta)){    
            lacunas[resp.index] = chute;
            erro = false;
        }
        
        if (erro){
            sprite.nextFrame();
        }
    };
    
    const ganhou = () => !lacunas.some(lacuna => lacuna == '');
    
    // const ganhou = () => lacunas.length
    //         ? !lacunas.some(function(lacuna){
    //             return lacuna == "";
    //         })
    //         : false;
    
    const perdeu = () => sprite.isFinished();
    
    const ganhouOuPerdeu = () => (ganhou() || perdeu());
    
    const reinicia = () => {
        palavraSecreta = "";
        lacunas = [];
        etapa = 1;
        sprite.reset();
    };
    
    // retorno
    return {
        setPalavraSecreta,
        getLacunas,
        getEtapa,
        processaChute,
        ganhou,
        perdeu,
        ganhouOuPerdeu,
        reinicia
    }
};
