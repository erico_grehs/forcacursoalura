const createSprite = seletor => {
    
    const moveFrame = (from, to) => 
        sprite.removeClass(from)
            .addClass(to);
    
    const hasNext = ()=> frameAtual+1 <= frameUltimo;
    
    const nextFrame = () => {
        if (hasNext()) moveFrame(frames[frameAtual],frames[++frameAtual]);            
    };
    
    const reset = () => {
        moveFrame(frames[frameAtual],frames[0]);
        frameAtual = 0;
    };
    
    const isFinished = () => !hasNext();
    
    const sprite = $(seletor);

    const frames = [
        "frame1","frame2","frame3","frame4","frame5",
        "frame6","frame7","frame8","frame9"
        ];
    
    let frameAtual = 0;
    let frameUltimo = frames.length -1;
    
    sprite.addClass(frames[frameAtual]);
    
    return {
        nextFrame,
        reset,
        isFinished
    };
};

