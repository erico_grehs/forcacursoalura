const criaController = jogo => {

    const $entrada = $('#entrada'); //denota objeto jQuery
    const $lacunas = $('.lacunas');

    const exibeLacunas = () => {
        $lacunas.empty();
        jogo.getLacunas().forEach(lacuna => 
            $("<li>")
                .addClass("lacuna")
                .text(lacuna)
                .appendTo($lacunas)
            );
    };

    const mudaPlaceHolder = texto => $entrada.attr("placeholder",texto);

    const guardaPalavraSecreta = () => {
        try {
            jogo.setPalavraSecreta($entrada.val().trim());
            $entrada.val("");
            mudaPlaceHolder("Chute");
            exibeLacunas();
        } catch (err) {
            alert(err.message);
        }
    };

    const leChute = () => {
        try {
            jogo.processaChute($entrada.val().trim().substr(0,1));
            $entrada.val("");
            exibeLacunas();
            setTimeout(() => {
                if(jogo.ganhouOuPerdeu()){
                    if (jogo.ganhou()){
                        alert("Ganhou!");
                    }
                    else{
                        alert("Perdeu!");
                    }
                    reiniciaController();
                }
            },200);    
        } catch (err) {
            alert(err.message);
        }
    };

    const reiniciaController = () => {
        $lacunas.empty();
        mudaPlaceHolder("Palavra secreta");
        jogo.reinicia();
    };

    const inicia = () => {
        $entrada.keypress(event => {
            if (event.which == 13) {
                switch (jogo.getEtapa()) {
                    case 1:
                        guardaPalavraSecreta();
                        break;
                    case 2:
                        leChute();
                        break;
                }
            }
        });
    };
    
    return { inicia};
};